﻿using DemoWPF.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWPF.Models
{
    public class Tache : ITache
    {
        private string _intitule;
        public string Intitule
        {
            get
            {
                return _intitule.ToUpper();
            }

            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new Exception("la valeur est incorrecte");
                }
                _intitule = value;
            }
        }

        public bool Accomplie { get; set; }

        public void Imprimer()
        {
            string nomF = Intitule + ".txt";
            using (FileStream fs = File.Create(nomF));
            File.WriteAllText(nomF, $"Intitule: {Intitule}\nAccompli: {(Accomplie ? "OK" : "KO")}");
        }
    }
}
