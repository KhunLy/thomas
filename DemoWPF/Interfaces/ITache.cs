﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWPF.Interfaces
{
    public interface ITache
    {
        string Intitule { get; set; }
        bool Accomplie { get; set; }
        void Imprimer();
    }
}
