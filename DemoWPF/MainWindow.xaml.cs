﻿using DemoWPF.Interfaces;
using DemoWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DemoWPF
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ITache Tache { get; set; }

        public ObservableCollection<ITache> Taches { get; }

        public MainWindow()
        {
            InitializeComponent();
            Tache = new Tache();
            Taches = new ObservableCollection<ITache>();
            DGTaches.ItemsSource = Taches;
        }

        private void Ajouter(object sender, RoutedEventArgs e)
        {
            string i = TBIntitule.Text;
            bool a = CBAccompli.IsChecked ?? false;
            try
            {
                Tache.Intitule = i;
                Tache.Accomplie = a;
                Taches.Add(Tache);
                Tache = new Tache();
                TBIntitule.Text = null;
                CBAccompli.IsChecked = false;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Imprimer(object sender, RoutedEventArgs e)
        {
            string i = TBIntitule.Text;
            bool a = CBAccompli.IsChecked ?? false;
            try
            {
                Tache.Intitule = i;
                Tache.Accomplie = a;
                Tache.Imprimer();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
